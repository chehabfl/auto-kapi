Auto-kapi
======

## Objectives

Automate the creation of resources on kapilands (yeah, I should have other things to do, but once every two month is fine...).

Known to work for electricity production.

## How to

Install python requirements:
```sh
pip install -r requirements.txt
```

Add a `config.json` file with the following structure:
```json
{
    "login": "YourLogin",
    "password": "YourPassword"
}
```

Run the script:
```sh
python ./process.py
```
