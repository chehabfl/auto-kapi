import json
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from time import sleep


with open("./config.json") as f:
    IDS = json.load(f)


browser = webdriver.Firefox()
browser.get('http://www.kapilands.fr/')

# Open the login form
browser.execute_script('anaml()')

sleep(0.5)
login_user = browser.find_element_by_name('USR')
login_pass = browser.find_element_by_name('pass')

login_user.send_keys(IDS['login'])
login_pass.send_keys(IDS['password'] + Keys.ENTER)


def find_href(test):
    for el in browser.find_elements_by_tag_name('a'):
        try:
            href = el.get_attribute("href")
            if test in href:
                return href
        except:
            pass
    return None


def find_hrefs(test):
    out = []
    for el in browser.find_elements_by_tag_name('a'):
        try:
            href = el.get_attribute("href")
            if test in href:
                out.append(href)
        except:
            pass
    return out


def navigate_to_prod_page():
    sleep(0.5)
    prod_page = find_href('page=gebs')
    if prod_page is None:
        raise Exception("errre")

    browser.get(prod_page)


def get_list_factory_href():
    l = find_hrefs('page=roh&art')
    if len(l) == 0:
        raise Exception("no factory")
    return l


navigate_to_prod_page()


curr_factory = 0
nb_factory = len(get_list_factory_href())

while curr_factory < nb_factory:
    sleep(1)
    browser.get(get_list_factory_href()[curr_factory])
    sleep(1)
    amount = browser.find_element_by_name('a_bestellen[]')
    amount.send_keys(Keys.BACKSPACE + '999999999' + Keys.ENTER)
    sleep(1)
    navigate_to_prod_page()
    curr_factory += 1
